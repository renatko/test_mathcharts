package test.test.mathgraph;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.MotionEvent;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.utils.MPPointD;

import java.util.ArrayList;
import java.util.List;

import test.test.mathgraph.math.Lagrange;
import test.test.mathgraph.math.LeastSquares;

public class MainActivity extends AppCompatActivity {

    SparseArray<Double> points = new SparseArray<>();

    LineChart chart;

    /*
    * for section from 0 to 6 will be 60 points with count 0.1
    * */
    static final int MAX_X = 60;
    static final float MULTIPLIER = .1f;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chart = findViewById(R.id.chart);

        setFirstTwoDotsForShowSomeData();

        setAxises();
        configChart();

        refreshData();
    }

    private void setFirstTwoDotsForShowSomeData(){
        points.put(1, 3d);
        points.put(2, 2d);
    }

    private List<Entry> getDataLeastSquares(){
        List<Entry> result = new ArrayList<>();
        if(points.size() > 3) {
            LeastSquares nmk = new LeastSquares();
            nmk.setPointsForCalculate(points);

            float x;
            for (int i = 0; i <= MAX_X; i++) {
                x = i * MULTIPLIER;
                result.add(new Entry(x, nmk.getMnkResultY(x)));
            }
        }
        return result;
    }

    private List<Entry> getDataLagrange(){
        List<Entry> result = new ArrayList<>();
        if(points.size() > 1) {
            Lagrange lagrange = new Lagrange();
            lagrange.setPointsForCalculate(points);
            float x;
            for (int i = 0; i <= MAX_X; i++) {
                x = i * MULTIPLIER;
                if (x % 1 == 0 && points.get((int)(x / 1)) != null) {
                    result.add(new Entry(x, points.get((int)(x / 1)).floatValue()));
                    continue;
                }
                result.add(new Entry(x, lagrange.getY(x)));
            }
        }

        return result;
    }

    private List<Entry> getDataPoints(){
        List<Entry> result = new ArrayList<>();
        int key;
        if(points.size() > 0) {
            for(int i = 0; i < points.size(); i++){
                key = points.keyAt(i);
                result.add(new Entry(key, points.get(key).floatValue()));
            }
        }
        return result;
    }

    private void setAxises(){
        XAxis xAxis = chart.getXAxis();
        xAxis.setAxisMaximum(6);

        YAxis yAxisLeft = chart.getAxisLeft();
        yAxisLeft.setAxisMaximum(10);

        YAxis yAxisRight = chart.getAxisRight();
        yAxisRight.setAxisMaximum(10);
    }

    private void configChart(){
        chart.setTouchEnabled(true);
        chart.setDragEnabled(false);
        chart.setScaleEnabled(false);
        chart.setDoubleTapToZoomEnabled(false);

        chart.setOnChartGestureListener(new OnChartGestureListener() {
            @Override
            public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

            }

            @Override
            public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
            }

            @Override
            public void onChartLongPressed(MotionEvent me) {
            }

            @Override
            public void onChartDoubleTapped(MotionEvent me) {
            }

            @Override
            public void onChartSingleTapped(MotionEvent me) {
                MPPointD point = chart.getTransformer(YAxis.AxisDependency.LEFT)
                        .getValuesByTouchPoint(me.getAxisValue(0), me.getAxisValue(1));
                double xValue = point.x;
                double yValue = point.y;

                int xInfelicity = (int)((xValue * 100) % 100);
                int x = (int)Math.round(xValue);

                if((xInfelicity > 90 || xInfelicity < 10) && (x < 6 && x > 0)){
                    addNewPoint(x, yValue);
                }
            }

            @Override
            public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
            }

            @Override
            public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
            }

            @Override
            public void onChartTranslate(MotionEvent me, float dX, float dY) {
            }
        });
    }

    void addNewPoint(int x, double y){
        points.put(x, y);
        refreshData();
    }

    void refreshData(){
        List<Entry> entriesSqaures = getDataLeastSquares();
        List<Entry> entriesLagrange = getDataLagrange();
        List<Entry> entriesPoints = getDataPoints();

        LineDataSet dataSetSquares = new LineDataSet(entriesSqaures, "Method of minimum Squares");
        dataSetSquares.setColor(Color.RED);
        dataSetSquares.setDrawCircles(false);
        dataSetSquares.setLineWidth(3);

        LineDataSet dataSetLagrange = new LineDataSet(entriesLagrange, "Lagrange");
        dataSetLagrange.setColor(Color.YELLOW);
        dataSetLagrange.setDrawCircles(false);
        dataSetLagrange.setLineWidth(3);

        LineDataSet dataSetPoints = new LineDataSet(entriesPoints, "Points");
        dataSetPoints.setColor(Color.TRANSPARENT);
        dataSetPoints.setLineWidth(0);
        dataSetPoints.setCircleColor(Color.BLUE);
        dataSetPoints.setLabel(null);


        List<ILineDataSet> lines = new ArrayList<>();
        if(entriesSqaures.size() > 0) lines.add(dataSetSquares);
        if(entriesLagrange.size() > 0) lines.add(dataSetLagrange);
        if(entriesPoints.size() > 0) lines.add(dataSetPoints);

        LineData linesSet = new LineData(lines);
        linesSet.setDrawValues(false);

        chart.setData(linesSet);

        chart.invalidate();
        chart.notifyDataSetChanged();
    }

}