package test.test.mathgraph.math;

import android.util.SparseArray;

/**
 * Created by Renat on 02.03.2018.
 */

public class Lagrange {

    private double[] xs = new double[0];
    private double[] ys = new double[0];

    public Lagrange(){}

    public void setPointsForCalculate(SparseArray<Double> points){
        xs = new double[points.size()];
        ys = new double[points.size()];

        int key;
        for(int i = 0; i < points.size(); i++){
            key = points.keyAt(i);
            xs[i] = key;
            ys[i] = points.get(key).floatValue();
        }
    }

    public float getY(double x){
        int size = xs.length;
        double sum = 0, term;

        for(int i = 0; i < size; ++i) {
            term = ys[i];
            for(int j = 0; j < size; ++j) {
                if( i != j)
                    term *= (x - xs[j]) / (xs[i] - xs[j]);
            }
            sum += term;
        }
        return (float)sum;
    }
}
