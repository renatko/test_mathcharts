package test.test.mathgraph.math;

import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Renat on 02.03.2018.
 */

public class LeastSquares {

    private double[] xs = new double[0];
    private double[] ys = new double[0];

    private double[] result = new double[0];

    private static final int n = 11;

    private List<List<Double>> listsX = new ArrayList<>();

    public LeastSquares(){}

    public void setPointsForCalculate(SparseArray<Double> points){

        xs = new double[points.size()];
        ys = new double[points.size()];

        int key;
        for(int i = 0; i < points.size(); i++){
            key = points.keyAt(i);
            xs[i] = key;
            ys[i] = points.get(key).floatValue();
        }

        calculateMethod();
    }

    public float getMnkResultY(double x){
        double sum = 0;
        for(int i = 0; i < result.length; i++){
            sum += result[i] * Math.pow(x, i);
        }
        return ((Number)sum).floatValue();
    }


    private void calculateMethod(){
        for(int index = 0; index < xs.length; index++) {
            List<Double> list = new ArrayList<>();
            double xVal = xs[index];
            double yVal = ys[index];
            for (int i = 0; i <= 6; i++) list.add(Math.pow(xVal, i));
            for (int i = 0; i <= 3; i++) list.add(yVal * Math.pow(xVal, i));
            listsX.add(list);
        }

        createListSum();
    }

    private void createListSum(){
        List<Double> listSum = new ArrayList<>();
        int indexMax = xs.length;
        double sum;
        for(int i = 0; i < n; i++){
            sum = 0;
            for(int j = 0; j < indexMax; j++){
                sum += listsX.get(j).get(i);
            }
            listSum.add(sum);
        }

        createMatrix(listSum);
    }

    private void createMatrix(List<Double> listSum){
        double[][] matrix = new double[4][5];
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 5; j++){
                if(j == 4){
                    matrix[i][j] = listSum.get(i + 7);
                }else{
                    matrix[i][j] = listSum.get(j + i);
                }
            }
        }

        getGaussResult(matrix);
    }

    private void getGaussResult(double[][] matrix){
        result = new Gauss().getGaussResult(matrix);
    }
}
