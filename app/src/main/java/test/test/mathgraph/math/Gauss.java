package test.test.mathgraph.math;


/**
 * Created by Renat on 02.03.2018.
 */

class Gauss {

    private static final double EPSILON = 1e-10;

    private double[][] A = new double[0][0];
    private double[] b = new double[0];

    Gauss(){
    }

    double[] getGaussResult(double[][] matrix){
        setMatrixForGauss(matrix);
        return calculate();
    }

    private void setMatrixForGauss(double[][] matrix){
        int lastIndex = matrix[0].length - 1;
        A = new double[matrix.length][lastIndex];
        for(int i = 0; i < A.length; i++){
            for(int j = 0; j < A[0].length; j++){
                A[i][j] = matrix[i][j];
            }
        }
        b = new double[matrix.length];
        for(int i = 0; i < b.length; i++){
            b[i] = matrix[i][lastIndex];
        }
    }

    private double[] calculate() {
        int n = b.length;

        for (int p = 0; p < n; p++) {

            int max = p;
            for (int i = p + 1; i < n; i++) {
                if (Math.abs(A[i][p]) > Math.abs(A[max][p])) {
                    max = i;
                }
            }

            double[] temp = A[p];
            A[p] = A[max];
            A[max] = temp;

            double t = b[p];
            b[p] = b[max];
            b[max] = t;

            if (Math.abs(A[p][p]) <= EPSILON) {
                throw new ArithmeticException("Matrix is singular or nearly singular");
            }

            for (int i = p + 1; i < n; i++) {
                double alpha = A[i][p] / A[p][p];
                b[i] -= alpha * b[p];
                for (int j = p; j < n; j++) {
                    A[i][j] -= alpha * A[p][j];
                }
            }
        }

        double[] x = new double[n];
        for (int i = n - 1; i >= 0; i--) {
            double sum = 0.0;
            for (int j = i + 1; j < n; j++) {
                sum += A[i][j] * x[j];
            }
            x[i] = (b[i] - sum) / A[i][i];
        }
        return x;
    }
}


